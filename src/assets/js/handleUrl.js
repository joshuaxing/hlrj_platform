export function getQueryString(name) {
  const reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
  let search = window.location.search;
  var r = null;
  if (search) {
      r = search.substring(1).match(reg);
  }
  if (r != null) {
      return r[2];
  }
  return null;
}