/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-08-23 10:15:51
 * @LastEditTime: 2019-08-23 14:49:39
 * @LastEditors: Please set LastEditors
 */
const CryptoJS = require('crypto-js');  //引用AES源码js

const key = CryptoJS.enc.Utf8.parse("IP@86SJFYR&PK9ky"); //十六位十六进制数作为密钥
const iv = CryptoJS.enc.Utf8.parse('H9@pk5PG9jsB&aSH'); //十六位十六进制数作为密钥偏移量

//解密方法
function Decrypt(word) {
  let decrypt = CryptoJS.AES.decrypt(word, key, { iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
  let decryptedStr = decrypt.toString(CryptoJS.enc.Utf8);
  return decryptedStr.toString();
}

//加密方法
function Encrypt(word) {
  let value = word;
  if (typeof (value) === 'object') {
    value = JSON.stringify(value);
  }
  let srcs = CryptoJS.enc.Utf8.parse(value);
  let encrypted = CryptoJS.AES.encrypt(srcs, key, { iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
  return encrypted.toString();
}

export {
  Decrypt,
  Encrypt
}
