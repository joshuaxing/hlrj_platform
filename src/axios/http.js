/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-08-20 00:26:49
 * @LastEditTime: 2019-08-26 09:49:44
 * @LastEditors: Please set LastEditors
 */
import axios from 'axios'
import qs from 'qs';
import {Decrypt, Encrypt} from '@/assets/js/secret'
axios.defaults.baseURL = process.env.NODE_ENV === 'production' ? '/' : '/api';
let arg = {}
let http = {};
http.post = function (api, params) {
  //编码
  const encryptParams = Encrypt(params);
  // console.log(encryptParams)
  const value = {
    param: encryptParams
  }
  return new Promise((resolve, reject) => {
    axios.post(api, value, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        transformRequest: [
          function (data, headers) {
            const paramstr = qs.stringify(data)
            return paramstr;
          }
        ],
        timeout: 20000
      })
      .then((response) => {
        //解码
        resolve(JSON.parse(Decrypt(response.data)))
      })
      .catch((error) => {
        reject(error.message);
      })
  })
}
// http request 拦截器
axios.interceptors.request.use(
  config => {
    return config;
  },
  err => {
    return Promise.reject(err);
  });

// http response 拦截器
axios.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    if (error && error.response) {
      switch (error.response.status) {
        case 400:
          error.message = '请求错误(400)';
          break;
        case 401:
          error.message = '(401)';
          break;
        case 403:
          error.message = '拒绝访问(403)';
          break;
        case 404:
          error.message = '请求出错(404)';
          break;
        case 408:
          error.message = '请求超时(408)';
          break;
        case 500:
          error.message = '服务器错误(500)';
          break;
        case 501:
          error.message = '服务未实现(501)';
          break;
        case 502:
          error.message = '网络错误(502)';
          break;
        case 503:
          error.message = '服务不可用(503)';
          break;
        case 504:
          error.message = '网络超时(504)';
          break;
        case 505:
          error.message = 'HTTP版本不受支持(505)';
          break;
        default:
          error.message = `连接出错(${error.response.status})!`;
      }
    } else {
      error.message = '网络不稳定,请稍后重试'
    }
    return Promise.reject(error);
  });

export default http;
