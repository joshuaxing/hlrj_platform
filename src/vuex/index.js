import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    collapse: false,
    menus: [],
    tagsList: [],
    adminId: 0,
    name: '管理员',
    uuadmin: {
      areaid: 0,
      cityid: 793,
      adminid: 0,
      name: ''
    }
  },
  getters: {

  },
  mutations: {
    tags(state, payload) {
      const data = payload.value;
      let arr = [];
      for (let i = 0, len = data.length; i < len; i++) {
          data[i].name && arr.push(data[i].name);
      }
      state.tagsList = arr;
    },
    iscollapse(state, payload) {
      state.collapse = !state.collapse
    },
    gotAdminInfo(state, payload) {
      state.uuadmin = payload.data;
    }
  },
  actions: {

  }
})

export default store
