import Vue from 'vue';
import App from './App';
import router from './router';
import store from './vuex'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';    // 默认主题
// import '../static/css/theme-green/index.css';       // 浅绿色主题
import '../static/css/icon.css';
import "babel-polyfill";
import animated from 'animate.css'
Vue.use(animated)
Vue.use(ElementUI, {size: 'small'});

new Vue({
    store,
    router,
    render: h => h(App)
}).$mount('#app');