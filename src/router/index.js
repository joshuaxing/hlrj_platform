/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-08-26 09:59:23
 * @LastEditTime: 2019-08-27 18:11:14
 * @LastEditors: Please set LastEditors
 */
import Vue from 'vue';
import Router from 'vue-router';
import store from '../vuex/index'
Vue.use(Router);
let router = new Router({
    mode: 'history',
    routes: [{
            path: '/',
            redirect: '/orindex'
        },
        {
            path: '/',
            component: resolve => require(['../components/page/Home/Index.vue'], resolve),
            meta: {
                title: '',
                requiresAuth: true
            },
            children: [
                {
                    path: '/check',
                    component: resolve => require(['../components/page/Check/index.vue'], resolve),
                    meta: {
                        title: '审核列表',
                        requiresAuth: true
                    }
                },
                {
                    path: '/mawedding',
                    component: resolve => require(['../components/page/Manage/wedding.vue'], resolve),
                    meta: {
                        title: '婚庆商户',
                        requiresAuth: true
                    }
                },
                {
                    path: '/macar',
                    component: resolve => require(['../components/page/Manage/car.vue'], resolve),
                    meta: {
                        title: '婚车管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/masheying',
                    component: resolve => require(['../components/page/Manage/sheying.vue'], resolve),
                    meta: {
                        title: '摄影管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/masiyi',
                    component: resolve => require(['../components/page/Manage/siyi.vue'], resolve),
                    meta: {
                        title: '司仪管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/magenzhuang',
                    component: resolve => require(['../components/page/Manage/genzhuang.vue'], resolve),
                    meta: {
                        title: '跟妆管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/maproduct',
                    component: resolve => require(['../components/page/Manage/product.vue'], resolve),
                    meta: {
                        title: '产品管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/macase',
                    component: resolve => require(['../components/page/Manage/case.vue'], resolve),
                    meta: {
                        title: '案例管理',
                        requiresAuth: true
                    }
                },

                {
                    path: '/orindex',
                    component: resolve => require(['../components/page/Order/order.vue'], resolve),
                    meta: {
                        title: '订单列表',
                        requiresAuth: true
                    }
                },
                {
                    path: '/suggest',
                    name: 'suggest',
                    component: resolve => require(['../components/page/Manage/suggest.vue'], resolve),
                    meta: {
                        title: '商家建议',
                        requiresAuth: true
                    },
                },
            ]
        },
        {
            path: '/login',
            component: resolve => require(['../components/page/Admin.vue'], resolve)
        },
        {
            path: '*',
            redirect: '/404'
        },
        {
            path: '/404',
            component: resolve => require(['../components/page/404.vue'], resolve)
        }
    ]
})

router.beforeEach((to, from, next) => {
    if (window.sessionStorage.getItem('xuuadmin')) {
      //登录信息
      store.commit({
        type: 'gotAdminInfo',
        data: JSON.parse(window.sessionStorage.getItem('xuuadmin'))
      })
    }
    if (to.matched.some(record => record.meta.requiresAuth)) {
        const id = store.state.uuadmin.adminid;
        if (id) {
          next()
        } else {
          next('/login')
        }
    } else {
      next() // 确保一定要调用 next()
    }
})

export default router;
