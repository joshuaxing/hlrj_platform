## Installation steps ##
	cd vue-manage-system											// Enter template directory
	npm install													// Installation dependency

## Local development ##

	// Open server and access http://localhost:8080 in browser
	npm run dev

## Constructing production ##

	// Constructing project
	npm run build